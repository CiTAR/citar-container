FROM eaas/eaas-appserver
CMD ["/sbin/my_init"]

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
	build-essential \
	python \
	file \
	&& apt-get clean && apt-get autoremove

ARG VERSION=2.4.2

RUN cd /tmp \
	&& wget https://github.com/singularityware/singularity/releases/download/$VERSION/singularity-$VERSION.tar.gz \
	&& tar xvf singularity-$VERSION.tar.gz \
	&& cd /tmp/singularity-$VERSION \
	&& ./configure && make install \
	&& cd / && rm -rf /tmp/singularity-$VERSION
